<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".deletepasswdentry").click(function(){ return confirm("Are you sure you want to delete this entry?")});
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"editpasswdentry", "deletepasswdentry", "editpasswd", "editfile"}, session) %>
<% htmlviewfunctions.displaycommandresults({"createpasswdentry"}, session, true) %>

<% local filename = cfe({ type="hidden", value=view.value.filename.value }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action.."?filename="..html.url_encode(view.value.filename.value) }) %>

<%
local editable = false
if view.value.mode and string.match(view.value.mode.value, "^.w") then
	editable = true
end
%>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<% htmlviewfunctions.displayitem(view.value.filename) %>
<% if view.value.mode then htmlviewfunctions.displayitem(view.value.mode) end %>
<% if view.value.data then %>
<% local containspasswd = 0 %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
<% for i,f in ipairs(view.value.fields.value) do %>
		<% if f == "Crypt-Password" then containspasswd = i end %>
		<th><%= html.html_escape(f) %></th>
<% end %>
	</tr>
</thead><tbody>
<% local entry = cfe({ type="hidden", value="" }) %>
<% for i,r in ipairs( view.value.data.value ) do %>
	<tr>
		<td>
<% if editable then %>
		<% entry.value = i %>
		<% if viewlibrary.check_permission("editpasswdentry") then %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, entry=entry, redir=redir}, label="", option="Edit", action="editpasswdentry"}), page_info, -1) %>
		<% end %>
		<% if viewlibrary.check_permission("deletepasswdentry") then %>
			<% htmlviewfunctions.displayitem(cfe({type="form", value={filename=filename, entry=entry, redir=redir}, label="", option="Delete", action="deletepasswdentry", class="deletepasswdentry"}), page_info, -1) %>
		<% end %>
		<% if 0 < containspasswd and r[containspasswd] ~= "" and viewlibrary.check_permission("editpasswd") then %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, entry=entry, redir=redir}, label="", option="Change Pass", action="editpasswd"}), page_info, -1) %>
		<% end %>
<% end %>
		</td>
<% for j,f in ipairs(r) do %>
		<td><% if (j == containspasswd) and (f ~= "") then io.write("********") else io.write(html.html_escape(f)) end %></td>
<% end %>
	</tr>
<% end %>
</tbody></table>
<% end %>


<% if editable and view.value.data and viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("editfile") then
	htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, redir=redir}, label="Expert", option="Edit", action="editfile"}), page_info, htmlviewfunctions.incrementheader(header_level))
end %>

<% if editable and view.value.data and viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("createpasswdentry") then
	local createform = viewlibrary.dispatch_component("createpasswdentry", {filename=view.value.filename.value, redir=redir.value}, true)
	createform.action = page_info.script .. page_info.prefix .. page_info.controller .. "/createpasswdentry"
	htmlviewfunctions.displayitem(createform, page_info, htmlviewfunctions.incrementheader(header_level))
end %>
<% htmlviewfunctions.displaysectionend(header_level) %>
