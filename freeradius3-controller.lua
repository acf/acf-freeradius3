local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.get_status()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.listfiles(self)
	return self.model.list_files()
end

function mymodule.editfile(self)
	return self.handle_form(self, self.model.get_file, self.model.update_file, self.clientdata, "Save", "Edit File", "File Saved")
end

function mymodule.createfile(self)
	return self.handle_form(self, self.model.getnewfile, self.model.createfile, self.clientdata, "Create", "Create New FreeRADIUS File", "FreeRADIUS File Created")
end

function mymodule.deletefile(self)
	return self.handle_form(self, self.model.getdeletefile, self.model.deletefile, self.clientdata, "Delete", "Delete FreeRADIUS File", "FreeRADIUS File Deleted")
end

function mymodule.listpasswdfiles(self)
	return self.model.list_passwd_files()
end

function mymodule.viewpasswdfile(self)
	return self.model.view_passwd_file(self, self.clientdata)
end

function mymodule.editpasswdentry(self)
	return self.handle_form(self, self.model.get_passwd_entry, self.model.update_passwd_entry, self.clientdata, "Save", "Edit Passwd Entry", "Entry Saved")
end

function mymodule.createpasswdentry(self)
	return self.handle_form(self, self.model.get_new_passwd_entry, self.model.create_passwd_entry, self.clientdata, "Create", "Create Passwd Entry", "Entry Created")
end

function mymodule.deletepasswdentry(self)
	return self.handle_form(self, self.model.get_delete_passwd_entry, self.model.delete_passwd_entry, self.clientdata, "Delete", "Delete Passwd Entry", "Entry Deleted")
end

function mymodule.editpasswd(self)
	return self.handle_form(self, self.model.get_passwd, self.model.update_passwd, self.clientdata, "Save", "Edit Password", "Password Saved")
end

function mymodule.listmacauthfiles(self)
	return self.model.list_macauth_files()
end

function mymodule.editmacauthfile(self)
	return self.handle_form(self, self.model.get_macauth_file, self.model.update_macauth_file, self.clientdata, "Save", "Edit File", "File Saved")
end

function mymodule.logfile(self)
	return self.model.get_logfile(self, self.clientdata)
end

return mymodule
