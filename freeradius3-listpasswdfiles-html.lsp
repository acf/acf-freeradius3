<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Passwd Configuration"}), page_info) %>
<% htmlviewfunctions.displayinfo(view) %>
<% if #view.value>0 then %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>File</th>
		<th>Size</th>
		<th>Last Modified</th>
	</tr>
</thead><tbody>
<% local filename = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,file in ipairs( view.value ) do %>
	<tr>
		<td>
		<% filename.value = file.filename %>
		<% if viewlibrary.check_permission("viewpasswdfile") then %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, redir=redir}, label="", option="View", action="viewpasswdfile"}), page_info, -1) %>
		<% end %>
		</td>
                <td><%= html.html_escape(file.filename) %></td>
		<td><span class="hide"><%= html.html_escape(file.size or 0) %>b</span><%= format.formatfilesize(file.size) %></td>
		<td><%= format.formattime(file.mtime) %></td>
	</tr>
<% end %>
</tbody></table>
<% else %>
<p>No files found</p>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level) %>
