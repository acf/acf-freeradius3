<% local form, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<script type="text/javascript">
        if (typeof jQuery == 'undefined') {
                document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	var editEntry = '<td> \
		<a href="javascript:;"><img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/list-remove.png" width="16" height="16" title="Remove entry"></a> \
		</td>';
	var addEntry = '<tr><td> \
		<a href="javascript:;"><img src="<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/list-add.png" width="16" height="16" title="Insert entry"></a> \
		</td><td></td></tr>';

	function deleteLine(){
		$(this).parent().parent().replaceWith();
	}
	function addLine(){
		addLinks($(this).parent().parent().after('<tr><td><input type="text" value=""></td></tr>').next());
	}
	function addLinks(rows){
		rows.each(function(){
			$(this).prepend(editEntry);
			$(this).find("td > a:last").click(deleteLine);
			$(this).find("input").change(validateLine);
		});
	}
	function validateLine(){
		var regex = /^[0-9a-f]{12}$/;
		var text = $(this).val().trim().replace(/[-:\s]/g, "").toLowerCase();
		if (regex.test(text)) {
			text = text.match(/.{2}/g).join("-");
			$(this).val(text);
			$(this).removeClass("error");
		} else {
			$(this).addClass("error");
		}
	}
	function submitFile(){
		var success = true;
		var file = "";
		var lines = $("#entries").find("input").each(function(index){
			if (/^\s*$/.test($(this).val())) {
				$(this).parent().parent().replaceWith();
			} else {
				file = file + $(this).val() + "\n";
				if ($(this).hasClass("error")) {
					success = false;
				}
			}
		});
		$("input[name='filecontent']").val(file);
		if ( ! success ) {
			return false;
		}
	}

	$(function(){
		$("#entries").find("td:empty").parent().replaceWith();
		$("#entries").find("td").contents().each(function(){
			$(this).replaceWith("<input type='text' value='" + $(this).text() + "'>");
		});
		$("#entries").find("input").each(validateLine);
		addLinks($("#entries").find("tr"));
		$("#entries").prepend(addEntry).find("a:first").click(addLine);
		$("form").submit(submitFile);
	});
</script>

<% htmlviewfunctions.displaycommandresults({"editfile"}, session) %>

<%
local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Edit MAC Authentication File"}), page_info)
htmlviewfunctions.displayitem(form.value.filename)
htmlviewfunctions.displayitem(form.value.size)
htmlviewfunctions.displayitem(form.value.mtime)
if not form.value.filename.errtxt then
local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label="Entries"}), page_info, htmlviewfunctions.incrementheader(header_level))
	htmlviewfunctions.displayformstart(form, page_info)
	form.value.filename.type = "hidden"
	htmlviewfunctions.displayformitem(form.value.filename, "filename")
	form.value.filecontent.type = "hidden"
	htmlviewfunctions.displayformitem(form.value.filecontent, "filecontent")
%>
<table id="entries">
<% for line in string.gmatch(html.html_escape(form.value.filecontent.value), "([^\n]*)\n?") do %>
	<tr><td><%= line %></td></tr>
<% end %>
</table>
<%
	htmlviewfunctions.displayinfo(form.value.filecontent)
	htmlviewfunctions.displayformend(form)
	htmlviewfunctions.displaysectionend(header_level2)
end
htmlviewfunctions.displaysectionend(header_level)
%>

<% if not form.value.filename.errtxt and viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("editfile") then
	local filename = cfe({ type="hidden", value=form.value.filename.value })
	local redir = cfe({ type="hidden", value=page_info.orig_action.."?filename="..html.url_encode(form.value.filename.value) })
	if form.value.redir then
		redir.value = redir.value.."&redir="..html.url_encode(form.value.redir.value)
	end
	htmlviewfunctions.displayitem(cfe({type="link", value={filename=filename, redir=redir}, label="Expert", option="Edit", action="editfile"}), page_info, htmlviewfunctions.incrementheader(header_level))
end %>
